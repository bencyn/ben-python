costs = [15, 10, 15, 20]

def sumCart(items):
    totalCost = 0

    for x in costs:
        totalCost += x
    return totalCost

print "Total sum %s" % sumCart(costs)

string = "Hello World"
print len(string)

number =10.6
num= "10e3"
z= -87.7e3
print int(number)
print float(num)
print z

# range
rng = range(2,11)
# declare odd numbers using range
odd_range= range(1,20,2)
print rng
print "odd numbers in range %s" % odd_range
