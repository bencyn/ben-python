import psycopg2

def connect():
    conn = None

    try:
        conn = psycopg2.connect(host="localhost", database="dvdrentals",user="postgres", password="ben742285")
    #     create a cursor
        cur = conn.cursor()
        print ('PostgreSQL database version:')
        cur.execute('SELECT version()')
    #     display the PostgreSQl database server version
        db_version = cur.fecthone()
        print(db_version)

    #     close the communication with the PostgreSQL
        cur.close()

    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')

if __name__=='__main__':
    connect()