import paramiko
from scp import SCPClient
import sys

# local_path= "C:\Users\Bencyn\Documents\python-scripts"
command = 'ls '
dir = '/var/www/html/blink'
dir2 = '/home/bencyn/'
command_dir = command + dir2


# command_dir='locate text2.txt'
# Define progress callback that prints the current percentage completed for the file

def progress(filename, size, sent):
    sys.stdout.write("%s\'s progress: %.2f%%   \r" % (filename, float(sent) / float(size) * 100))


def sshServer(hostname, port, username, password, command_dir):
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy)
    ssh.load_system_host_keys()
    ssh.connect(hostname, port, username, password)

    scp = SCPClient(ssh.get_transport(), progress=progress)

    # scp.put('hello.txt',recursive=True,remote_path='~/home/bencyn/')
    scp.put('test.txt', '~/test.txt')
    scp.close()

    # stdin, stdout, stderr = ssh.exec_command(command_dir)
    # print(stdout.read())
    # with SCPClient(ssh.get_transport()) as scp:
    #     scp.put('ben.txt', 'ben2.txt')
    #     scp.get('ben2.txt')


if __name__ == '__main__':
    sshServer('178.128.185.54', 22, 'root', 'Akish@9420', command_dir)
